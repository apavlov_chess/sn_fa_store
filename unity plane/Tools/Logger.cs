﻿

using System;

namespace Tools
{
    public static class Logger
    {
        public enum LogComponents : ushort
        {
            None = 0x0000,
            View = 0x0001,
            Network = 0x0002,
            Notice = 0x0004
        }

        public static void Log(LogComponents logComponents, string message)
        {

#if UNITY_EDITOR
            if (logComponents == LogComponents.Notice)
                UnityEngine.Debug.LogError(message);
            else
                UnityEngine.Debug.Log(message);
            //UnityEngine.Debug.Log("UNITY_EDITOR");
#elif UNITYFORM
            if (logComponents == LogComponents.Notice)
                UnityEngine.Debug.LogError(message);
            else
                UnityEngine.Debug.Log(message);
            //UnityEngine.Debug.LogWarning(message);
            //UnityEngine.Debug.Log("UNITYFORM");
#elif UNITY_ANDROID
            if (logComponents == LogComponents.Notice)
                UnityEngine.Debug.LogError(message);
            else
             UnityEngine.Debug.Log(message);
            //UnityEngine.Debug.Log("UNITY_ANDROID");
#else
            //   if (logComponents == LogComponents.Notice)
         //       UnityEngine.Debug.LogError(message);
         //   else
            System.Diagnostics.Debug.WriteLine(message,"Field");
            Console.WriteLine(message);
#endif

        }
    }


}
