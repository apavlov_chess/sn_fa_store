﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Tools
{
    public class ThreadExecutor
    {
        private static ThreadExecutor _threadExecutor;
        private Object _syncObj = new Object();
        private bool _isRunning;
        private readonly Queue<Action> _queue = new Queue<Action>();
        //private readonly AutoResetEvent _locker = new AutoResetEvent(false); // without this some time times get blocked ; toask
        private Thread _thread;
        public static ThreadExecutor Instance
        {
            get
            { return _threadExecutor ?? (_threadExecutor = new ThreadExecutor()); }
        }

        private ThreadExecutor()
        {
            _thread = new Thread(Run);
            _isRunning = true;
            _thread.Start(); 
        }

        public void PushTusk(Action action)
        {
            lock (_syncObj)
                _queue.Enqueue(action);
          //  _locker.Set();
        }

        private void Run()
        {
            while (_isRunning)
            {

                //while
                    if (_queue.Count > 0)
                {
                    Action action;
                    lock (_syncObj)
                        action = _queue.Dequeue();
                    action();
                }
               // _locker.WaitOne();
            }
        }

       public void Abort()
        {
            _isRunning = false;
          //  _locker.Set();
            _thread.Join();
        }
    }
}
