﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tools;
using UnityEngine;

namespace UnityView
{
    class ScreenTouchController : MonoBehaviour
    {
        public Camera camera;
        private void Update()
        {


            if (Input.touchCount == 2)
            {
                Touch touch0 = Input.GetTouch(0);
                Touch touch1 = Input.GetTouch(1);
                if (touch0.phase == TouchPhase.Moved && touch1.phase == TouchPhase.Moved)
                {

                    //You need two fingers on screen to pinch.

                    //First set the initial distance between fingers so you can compare.

                    Vector2 prevDist = (touch0.position - touch0.deltaPosition) -
                                       (touch1.position - touch1.deltaPosition);
                    Vector2 curDist = touch0.position - touch1.position;
                    float delta = curDist.magnitude - prevDist.magnitude;

                    if (Mathf.Abs(delta) > 1.8f)
                    {

                        if (camera.fieldOfView > 10 && camera.fieldOfView < 170)
                          camera.fieldOfView -= delta;
                      
                          if ( camera.fieldOfView < 10 && delta < 0)
                              camera.fieldOfView -= delta;
                      
                        if (camera.fieldOfView > 170 && delta > 0)
                            camera.fieldOfView -= delta;
                        
                      Logger.Log(Logger.LogComponents.Notice, " camera" + camera.fieldOfView +" "+delta);
                    }
                    else
                    {
                        float koef = camera.fieldOfView/(float) 200;
                        //float koef1 = (camera.fieldOfView) / ((camera.fieldOfView - 155) * (camera.fieldOfView - 155));
                        camera.transform.localPosition = new Vector3(
                            camera.transform.localPosition.x - touch0.deltaPosition.x * koef * koef,
                            camera.transform.localPosition.y - touch0.deltaPosition.y * koef * koef,
                            camera.transform.localPosition.z);
                        Logger.Log(Logger.LogComponents.Notice,  "koef1  " + koef);
                    }


                   
                }
            }
        }
    }
}
