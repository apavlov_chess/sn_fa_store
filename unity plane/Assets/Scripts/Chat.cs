﻿
using System.Collections;
using Tools;
using UnityEngine;

namespace UnityView
{

    public class Chat : MonoBehaviour
    {
        public GameObject Hex1;
        public Transform _transform;
        private GameObject[,] _hexStorage = new GameObject[51, 51]; 
        Color c13;
        private int _centerX = 26-1;
        private int _centerY = 26-1;
        private float _HexWidth = (1.73f + 0.01f);
        private float _HexHeight = (1.5f + 0.01f);
        private bool start = true;
        private void Start()
        {
            
            //c1 = Hex1.renderer.material.color;//
            //c2 = Hex2.renderer.material.color;//
            //c3 = Hex3.renderer.material.color;//
           
        }

        private void Update()
        {
            if (Input.GetKey(KeyCode.Escape) && start)
            {
                start = false;
                
                for (int x = 0; x < 51; x++)
                {
                    for (int y = 0; y < 51; y++)
                    {
                        float odd = 0;
                        if (y%2 == 0)
                            odd = _HexWidth/2;
                        Vector3 pos;
                       
                        pos = new Vector3(_HexWidth * (x - _centerX)+odd, 0, _HexHeight * (y - _centerY));
                        _hexStorage[x, y] = (GameObject) Instantiate(Hex1);//, pos, Quaternion.identity);
                        _hexStorage[x, y].transform.parent = _transform;
                        _hexStorage[x, y].transform.localScale = new Vector3(100, 100, -21);
                        _hexStorage[x, y].transform.localPosition = pos;
                        _hexStorage[x, y].transform.localEulerAngles= new Vector3(-90,180,0);
                        Logger.Log(Logger.LogComponents.Notice,
                            "  " + x + " " + y + "; " + _hexStorage[x, y].transform.position.x + " " +
                            _hexStorage[x, y].transform.position.y + " " + _hexStorage[x, y].transform.position.z);

                        //  myArr[i, j] = ran.Next(1, 15);
                        //  Console.Write("{0}\t", myArr[i, j]);
                    }
                    //c3.a = 0.8f;
                    //Hex3.renderer.material.color = c3;
                    //if (c1.a > 0.1)
                    //{
                    //    c1.a -= 0.1f;
                    //    Logger.Log(Logger.LogComponents.Notice, "c.a = " + c1.a);
                    //    Hex1.renderer.material.color = c1;
                    //}
                    //if (c2.a > 0.1)
                    //{
                    //    c2.a -= 0.1f;
                    //    Logger.Log(Logger.LogComponents.Notice, "c.a = " + c2.a);
                    //    Hex2.renderer.material.color = c2;
                    //}
                }
            }
        }

        void OnApplicationQuit()
        {
            Application.Quit();
           
            Logger.Log(Logger.LogComponents.Notice, "Application ending after " + Time.time + " seconds");
           
        }
    }
}
