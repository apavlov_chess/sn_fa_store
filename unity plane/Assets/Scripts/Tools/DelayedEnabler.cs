﻿using UnityEngine;

/// <summary>
///     Dirty hack for to panels on screen. Use to enable the second one on first Update
/// </summary>
public class DelayedEnabler : MonoBehaviour
{
    public GameObject ObjectToEnable;

    private void Update()
    {
        //enable  GameObject and all its children
        ObjectToEnable.SetActive(true);
        //self disabling (scipt only)
        enabled = false;
    }
}