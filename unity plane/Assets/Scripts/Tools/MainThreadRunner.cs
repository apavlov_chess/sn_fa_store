﻿using System;
using System.Threading;
using Tools;
using UnityEngine;
using System.Collections.Generic;

namespace UnityView.Tools
{
        public class MainThreadRunner : MonoBehaviour
    {

        private static readonly Queue<Action> _queue = new Queue<Action>();
        private static Thread _thread;
        private static MainThreadRunner _instance;
        private static System.Object _syncObj = new System.Object();
        private void Awake()
        {
            Application.targetFrameRate = 60;
            if (_instance != null)
                throw new Exception("Duplicating CoroutineStarter");
            _instance = this;
            _thread = Thread.CurrentThread;
            //     Logger.LogError(" width " + Screen.width + "height = " + Screen.height);
        }

        public static bool IsMainThread
        {
            get { return (_thread == Thread.CurrentThread); }
        }
        public static void PushTask(Action action)
        {
            if (IsMainThread)
            {
                action();
            }
            else
                lock (_syncObj)
                    _queue.Enqueue(action);
        }
        private void Update()
        {
            if (_queue.Count < 1)
                return;
            Action action;
            lock (_syncObj)
                action = _queue.Dequeue();
            action();
        }
    }
}
