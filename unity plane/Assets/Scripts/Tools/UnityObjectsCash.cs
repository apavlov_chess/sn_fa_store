﻿
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

namespace UnityView.Tools
{

    public class UnityObjectsCash
    {
        private string _cashStorageName = "ObjectCash";
        private GameObject _cashStorage;
        private static UnityObjectsCash _instance;
        private readonly List<MonoBehaviour> _elements = new List<MonoBehaviour>();

        
        private GameObject CacheStorage
        {
            get { return _cashStorage ?? (_cashStorage = GameObject.Find(_cashStorageName) ?? new GameObject(_cashStorageName)); }
        }
        private UnityObjectsCash()
        {
        }

        public static UnityObjectsCash Instance()
        {
            return _instance ?? (_instance = new UnityObjectsCash());
        }

        public void Add<T>(T item) where T : MonoBehaviour
        {
            _elements.Add(item);
            var  t= CacheStorage;
            item.transform.parent = t.transform;

        }

        public T Get<T>(MonoBehaviour prefab) where T : MonoBehaviour
        {
            MonoBehaviour t = _elements.Where(e => e.GetType() == typeof(T)).FirstOrDefault();
            if (t == null)
            {
                t = (MonoBehaviour) Object.Instantiate(prefab);
             
            }
            else
            {
                _elements.Remove(t);
                t.transform.parent = null;
            }

            return (T)t;
        }
    }
}
