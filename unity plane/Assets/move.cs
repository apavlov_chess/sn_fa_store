﻿using UnityEngine;
using System.Collections;


public class move : MonoBehaviour {
	public  Rigidbody rigidbody1;
	// Use this for initialization

float speed = 50.0f;
	/*
void Update () {
	
	if (Input.GetKey(KeyCode.A))
		{
			transform.Rotate(Vector3.up *speed* Time.deltaTime);
		Debug.LogWarning ("fg");
		}
	//if (Input.GetKey(KeyCode.D))
	//	transform.Rotate(-Vector3.up * speed * Time.deltaTime);
		var h = Input.GetAxis("Vertical"); // use the same axis that move back/forth
		var v = Input.GetAxis("Horizontal"); // use the same axis that turns left/right
		transform.localEulerAngles.x =  -v*60; // forth/back banking first!
		transform.localEulerAngles.z =   h*45;  // left/right
	
}
*/
	public float AmbientSpeed = 100.0f;
	
	public float RotationSpeed = 200.0f;
	void Update()
	{        Quaternion AddRot = Quaternion.identity;
		float roll = 0;
		//float pitch = 0;
		//float yaw = 0;
		roll   = Input.GetAxis("Horizontal")  * (Time.deltaTime * RotationSpeed);
	//	pitch = Input.GetAxis("Pitch") * (Time.deltaTime * RotationSpeed);
	//	yaw = Input.GetAxis("Yaw") * (Time.deltaTime * RotationSpeed);
		AddRot.eulerAngles = new Vector3(0, 0, -roll);
		rigidbody.rotation *= AddRot;
		Vector3 AddPos = Vector3.forward;
		AddPos = rigidbody1.rotation * AddPos;
		rigidbody.velocity = AddPos * (Time.deltaTime * AmbientSpeed);
	}
}